const boardFile = require(`${__dirname}/callback1.js`)
const listFile = require(`${__dirname}/callback2.js`)
const cardFile = require(`${__dirname}/callback3.js`)

function board(name){
    setTimeout(() => {
        boardFile.callback(name,function cb(boardValue){
            console.log(boardValue)
            listFile.callback(boardValue.id, (list)=>{
                let listId;
                console.log(list)
                for (let index = 0 ; index < list.length; index++){
                    for (let key in list[index]){
                        if (list[index][key] =='Mind'){
                            listId = list[index].id;
                            break
                        }
                    }
                }
                cardFile.callback(listId, (cards)=>{
                    console.log(cards)
                    
                })
            })
        
        })}, 2 * 1000)}
        
        
        module.exports = {board}; 