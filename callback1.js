const boards = require(`${__dirname}/data/boards.json`)

let callback = function(key, cb){
    let result;
    setTimeout(function () {
        for (let index = 0;index < boards.length; index++){
            let flag = 0;
            for (let boardsKey in boards[index]){
                if (boards[index][boardsKey] === key){
                    flag = 1;
                }
            }
            if (flag == 1 ){
                result = boards[index];
            }
        }
    return cb(result);
    }, 2 * 1000);
}

module.exports={callback}

