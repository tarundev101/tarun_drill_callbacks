const boardFile = require(`${__dirname}/callback1.js`)
const listFile = require(`${__dirname}/callback2.js`)
const cardFile = require(`${__dirname}/callback3.js`)

function board(name){
    setTimeout(() => {
    
    boardFile.callback(name,function cb(boardValue){
    console.log(boardValue)
    listFile.callback(boardValue.id, (wholeList) => {
        let listId = [];
        console.log(wholeList)
        for (let index = 0 ; index < wholeList.length; index++){
            for (let key in wholeList[index]){
                    listId.push(wholeList[index].id);
            }
        }
        for (let index = 0; index < listId.length; index++){
            cardFile.callback(listId[index], (cards)=>{
                console.log(cards)
                
            })
        }
    })
})}, 2 * 1000);}


module.exports = {board}; 