const cards = require(`${__dirname}/data/cards.json`)
function callback(listId, cb) {
    setTimeout(() => {
        return cb(cards[listId])
    }, 2 * 1000);
}
module.exports = {callback}; 
