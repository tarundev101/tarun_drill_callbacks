const list = require(`${__dirname}/data/lists.json`)

function callback( boardId, cb) {
    setTimeout(() => {
        return cb(list[boardId])
    }, 2 * 1000);
}
module.exports = {callback}; 